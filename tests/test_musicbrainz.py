# Calliope
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""Tests for the `cpe musicbrainz` command."""

import io
import logging
import os
import re
import urllib.error
import urllib.parse
import urllib.request

import pytest

musicbrainzngs = pytest.importorskip("musicbrainzngs")
import testutils


# Pretend to be a musicbrainz server.
#
# Adapted from here:
# https://github.com/alastair/python-musicbrainzngs/blob/master/test/_common.py
class FakeOpener(urllib.request.OpenerDirector):
    """Fake URL opener for providing a mock Musicbrainz server."""

    def __init__(self, response_set):
        self.myurl = None
        self.headers = None
        self.response_set = response_set

    def open(self, request, body=None):
        self.myurl = request.get_full_url()
        self.headers = request.header_items()
        self.request = request

        return self.response_set.open(request.get_full_url())

    def get_url(self):
        return self.myurl


class ResponseSet:
    """A table of responses for the fake server to provide.

    The response_table should be a list of tuples, where each tuple maps a
    regular expression, matched against the URL path and query, to a response.
    The response should be a string to be returned, or an Exception subclass
    to be raised.

    """

    def __init__(self, response_table):
        self.response_table = response_table
        self.log = logging.getLogger("mock musicbrainz server")

    def open(self, url):
        to_match = url

        for pattern, response in self.response_table:
            if pattern == to_match or re.match(pattern, to_match):
                self.log.debug("Matched URL %s against %s", url, pattern)
                if isinstance(response, Exception):
                    raise response
                return io.BytesIO(response.encode("utf-8"))
        else:
            self.log.warning("No URL patterns matched, returning 404")
            return urllib.error.HTTPError("", 404, "", "", io.StringIO(""))


def stock_response(filename):
    """Return a pre-downloaded Musicbrainz response from the data/ subdir."""
    data_path = os.path.join(os.path.dirname(__file__), "data", filename)
    with open(data_path) as f:
        return f.read()


def error_404():
    return urllib.error.HTTPError("", 404, "", "", io.StringIO(""))


@pytest.fixture()
def mock_server(monkeypatch):
    def set_responses(responses):
        monkeypatch.setattr(
            "musicbrainzngs.compat.build_opener",
            lambda *args: FakeOpener(ResponseSet(responses)),
        )

    return set_responses


@pytest.fixture()
def cli():
    return testutils.Cli(prepend_args=["--verbosity", "3", "musicbrainz"])


MB = r"https://musicbrainz.org/ws/2"
COVERART = r"https://coverartarchive.org"


def test_resolve_artist(cli, mock_server):
    mock_server(
        [
            (
                MB + r"/artist/\?limit=100&query=artist%3A%28r%C3%B6yksopp%29",
                stock_response("musicbrainz-search-artist.xml"),
            ),
        ]
    )

    playlist = [{"creator": "Röyksopp"}]

    result = cli.run(["annotate", "--output", "-", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()
    assert output == [
        {
            "creator": "Röyksopp",
            "musicbrainz.artist": "Röyksopp",
            "musicbrainz.artist_country": "NO",
            "musicbrainz.artist_id": "1c70a3fc-fa3c-4be1-8b55-c3192db8a884",
            "calliope.musicbrainz.resolver_score": 1.0,
        }
    ]


def test_resolve_album(cli, mock_server):
    mock_server(
        [
            (
                MB
                + "/release/?limit=100&query=artist%3A%28the+burning+hell%29+release%3A%28public+library%29",
                stock_response("musicbrainz.resolve_album.xml"),
            ),
        ]
    )

    playlist = [
        {
            "album": "Public Library",
            "creator": "The Burning Hell",
        }
    ]

    result = cli.run(["annotate", "--output", "-", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()
    assert output == [
        {
            "album": "Public Library",
            "creator": "The Burning Hell",
            "musicbrainz.album": "Public Library",
            "musicbrainz.albumartist": "The Burning Hell",
            "musicbrainz.artist_id": "4d55b744-b94d-44ef-bc99-15cd1cbe3cca",
            "musicbrainz.date": "2016-04-01",
            "musicbrainz.release_group_id": "466da3b9-a5a7-42ea-ab03-bdcaa64f41bd",
            "musicbrainz.release_id": "b8f7019c-a57d-4bd2-840a-b3ea80bcd32c",
            "calliope.musicbrainz.resolver_score": 0.9975124378109452,
        }
    ]


def test_resolve_track(cli, mock_server):
    mock_server(
        [
            (
                MB
                + r"/recording/\?limit=100&query=recording.*the\+light.*\+artist.*sbtrkt\+denai\+moore.*",
                stock_response("musicbrainz.search-recording.xml"),
            ),
        ]
    )

    playlist = [
        {
            "title": "The Light feat. Denai Moore",
            "creator": "SBTRKT",
            "album": "Wonder Where We Land",
            "duration": 186.408,
        }
    ]

    result = cli.run(
        ["annotate", "--update", "--output", "-", "-"], input_playlist=playlist
    )
    result.assert_success()

    output = result.json()
    assert output == [
        {
            "album": "Wonder Where We Land",
            "creator": "SBTRKT feat. Denai Moore",
            "duration": 186436.0,
            "musicbrainz.album": "Wonder Where We Land",
            "musicbrainz.albumartist": "SBTRKT",
            "musicbrainz.artist": "SBTRKT feat. Denai Moore",
            "musicbrainz.artist_id": "7f2aa196-cfd4-4a3d-ace3-28b7b6a79af7",
            "musicbrainz.date": "2014-09-24",
            "musicbrainz.isrcs": ["GBBKS1400215"],
            "musicbrainz.length": 186436.0,
            "musicbrainz.recording_id": "13b18d21-9600-419c-888e-99c9c5e1d9a3",
            "musicbrainz.release_group_id": "08c5a214-0628-4257-837d-f6e56a043631",
            "musicbrainz.title": "The Light",
            "title": "The Light",
            "calliope.musicbrainz.resolver_score": 0.9945150019690607,
        }
    ]


def test_resolve_track_with_isrc(cli, mock_server):
    mock_server(
        [
            (
                MB
                + r"/recording/\?limit=100&query=recording%3A%28like\+eating\+glass%29\+artist%3A%28bloc\+party%29",
                stock_response("musicbrainz.resolve_track_with_isrc.xml"),
            ),
        ]
    )

    playlist = [
        {
            "title": "Like Eating Glass",
            "creator": "Bloc Party",
            "musicbrainz.isrcs": ["QM6MZ1916107"],
        }
    ]

    result = cli.run(["annotate", "--output", "-", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()[0]

    assert output["musicbrainz.album"] == "Silent Alarm Live"
    assert output["musicbrainz.artist_id"] == "8c538f11-c141-4588-8ecb-931083524186"
    assert output["musicbrainz.isrcs"] == ["QM6MZ1916107"]
    assert output["musicbrainz.recording_id"] == "5667663d-a5f9-46cd-868b-ab8822e4f3ee"
    assert (
        output["musicbrainz.release_group_id"] == "274009ee-3e4d-476c-9a79-eed4d81efebb"
    )


def test_resolve_track_with_rid(cli, mock_server):
    mock_server(
        [
            (
                MB
                + r"/recording/?limit=100&query=rid%3A%285667663d%5C-a5f9%5C-46cd%5C-868b%5C-ab8822e4f3ee%29",
                stock_response("musicbrainz.resolve_track_with_rid.xml"),
            ),
        ]
    )

    playlist = [
        {
            "title": "foo",
            "musicbrainz.recording_id": "5667663d-a5f9-46cd-868b-ab8822e4f3ee",
        }
    ]

    result = cli.run(["annotate", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()[0]

    assert output["musicbrainz.album"] == "Silent Alarm Live"
    assert output["musicbrainz.artist_id"] == "8c538f11-c141-4588-8ecb-931083524186"
    assert output["musicbrainz.recording_id"] == "5667663d-a5f9-46cd-868b-ab8822e4f3ee"
    assert (
        output["musicbrainz.release_group_id"] == "274009ee-3e4d-476c-9a79-eed4d81efebb"
    )


def test_resolve_track_with_rgid(cli, mock_server):
    mock_server(
        [
            (
                MB
                + r"/recording/?limit=100&query=recording%3A%28like+eating+glass%29+rgid%3A%28274009ee%5C-3e4d%5C-476c%5C-9a79%5C-eed4d81efebb%29",
                stock_response("musicbrainz.resolve_track_with_rgid.xml"),
            ),
        ]
    )

    playlist = [
        {
            "title": "Like Eating Glass",
            "musicbrainz.release_group_id": "274009ee-3e4d-476c-9a79-eed4d81efebb",
        }
    ]

    result = cli.run(["annotate", "--output", "-", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()[0]

    assert output["musicbrainz.album"] == "Silent Alarm Live"
    assert output["musicbrainz.artist_id"] == "8c538f11-c141-4588-8ecb-931083524186"
    assert output["musicbrainz.recording_id"] == "5667663d-a5f9-46cd-868b-ab8822e4f3ee"
    assert (
        output["musicbrainz.release_group_id"] == "274009ee-3e4d-476c-9a79-eed4d81efebb"
    )


def test_resolve_image(cli, mock_server):
    mock_server(
        [
            (
                MB + r"/release/\?query=Richard\+AND\+Dawson\+AND\+2020",
                stock_response("musicbrainz-search-release.richard-dawson-2020.xml"),
            ),
            (
                COVERART + r"/release/2757d45c-3404-41db-92c2-a82a76d70363",
                stock_response("coverartarchive-release.richard-dawson-2020.xml"),
            ),
        ]
    )

    playlist = [{"creator": "Richard Dawson", "album": "2020"}]

    result = cli.run(["resolve-image", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()
    assert output == [
        {
            "creator": "Richard Dawson",
            "album": "2020",
            "musicbrainz.artist_id": "ee7923b9-5850-47b5-97cc-5cecdc21578d",
            "musicbrainz.release_id": "2757d45c-3404-41db-92c2-a82a76d70363",
            "musicbrainz.release_group_id": "feb16471-6a08-4ded-9b46-c9b76e8d90a2",
            "image": "http://coverartarchive.org/release/2757d45c-3404-41db-92c2-a82a76d70363/24066950835-250.jpg",
        }
    ]


def test_releases_invalid_album_mbid(cli, mock_server):
    """The playlist may specify an MBID that's incorrect."""

    playlist = [
        {
            "creator": "Alvvays",
            "album": "Alvvays",
            "title": "Atop a Cake",
            "musicbrainz.artist_id": "99450990-b24e-4132-bb68-235f8c3e2564",
            "musicbrainz.album_id": "0ea1ee01-54b6-439f-bf01-250375741813",
            "musicbrainz.identifier": "5d571740-6d0c-40d7-96fe-91391dbc91f5",
        }
    ]

    mock_server(
        [
            (MB + r"/release/0ea1ee01-54b6-439f-bf01-250375741813", error_404()),
        ]
    )

    result = cli.run(["annotate", "--include=release", "-"], input_playlist=playlist)
    assert isinstance(result.exception, musicbrainzngs.ResponseError)


def test_server_error(cli, mock_server):
    """Inject a 404 error and see how the client responds.

    For simplicity, the client doesn't handle these exceptions currently so
    we test for an unhandled musicbrainzngs.ResponseError().

    """
    mock_server([(".*", error_404())])

    playlist = [
        {
            "title": "The Light feat. Denai Moore",
            "creator": "SBTRKT",
            "album": "Wonder Where We Land",
            "duration": 186.408,
        }
    ]

    result = cli.run(["annotate", "-"], input_playlist=playlist)

    assert isinstance(result.exception, musicbrainzngs.ResponseError)
