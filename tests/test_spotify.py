import json
import os
from pathlib import Path
from typing import Dict

import pytest

import calliope
from tests import testutils


@pytest.fixture()
def cli():
    return testutils.Cli(prepend_args=["--verbosity=3", "spotify"])


def get_dummy_sp_search_paginated(responses):
    def dummy_search_paginated(
        api,
        query_str: str,
        item_type: str = "track",
        result_count_limit=300,
    ):
        json_path = Path(Path(__file__).parent, "data", responses[query_str])
        with open(json_path) as fd:
            return json.load(fd)

    return dummy_search_paginated


@pytest.fixture()
def mock_server(monkeypatch):
    def set_responses(responses):
        os.environ["CALLIOPE_SPOTIFY_MOCK"] = "yes"

        monkeypatch.setattr(
            calliope.spotify,
            "_search_paginated",
            get_dummy_sp_search_paginated(responses),
        )

    return set_responses


def test_resolve_track(cli, mock_server):
    mock_server(
        {
            "track:The Light artist:SBTRKT": "spotify.track:The Light artist:SBTRKT.json",
            "The Light SBTRKT": "spotify.The Light SBTRKT.json",
            "SBTRKT": "spotify.SBTRKT.json",
        }
    )

    playlist = [
        {
            "title": "The Light (feat. Denai Moore)",
            "creator": "SBTRKT",
            "album": "Wonder Where We Land",
        }
    ]

    result = cli.run(["resolve", "--update", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()
    assert output == [
        {
            "album": "Wonder Where We Land",
            "creator": "SBTRKT",
            "duration": 186436.0,
            "spotify.album": "Wonder Where We Land",
            "spotify.album_id": "4J9gt4YOazmavlYw4hMrfY",
            "spotify.albumartist": "SBTRKT",
            "spotify.artist": "SBTRKT",
            "spotify.artist_id": "1O10apSOoAPjOu6UhUNmeI",
            "spotify.date": "2014-09-29",
            "spotify.duration_ms": 186436.0,
            "spotify.id": "1lFz7QPoxqBdUV4iugS3MX",
            "spotify.isrc": "GBBKS1400215",
            "spotify.popularity": 38,
            "spotify.title": "The Light",
            "title": "The Light",
            "calliope.spotify.resolver_score": 0.977384780278671,
        }
    ]


def test_resolve_track_with_isrc(cli, mock_server):
    mock_server(
        {
            "isrc:GBBKS1400215": "spotify.isrc:GBBKS1400215.json",
            "track:The Light artist:SBTRKT": "spotify.track:The Light artist:SBTRKT.json",
            "The Light SBTRKT": "spotify.The Light SBTRKT.json",
            "SBTRKT": "spotify.SBTRKT.json",
        }
    )

    playlist = [
        {
            "title": "The Light (feat. Denai Moore)",
            "creator": "SBTRKT",
            "album": "Wonder Where We Land",
            "musicbrainz.isrcs": ["GBBKS1400215"],
        }
    ]

    result = cli.run(["resolve", "--update", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()
    assert output == [
        {
            "album": "Wonder Where We Land",
            "creator": "SBTRKT",
            "duration": 186436.0,
            "musicbrainz.isrcs": ["GBBKS1400215"],
            "spotify.album": "Wonder Where We Land",
            "spotify.album_id": "4J9gt4YOazmavlYw4hMrfY",
            "spotify.albumartist": "SBTRKT",
            "spotify.artist": "SBTRKT",
            "spotify.artist_id": "1O10apSOoAPjOu6UhUNmeI",
            "spotify.date": "2014-09-29",
            "spotify.duration_ms": 186436.0,
            "spotify.id": "1lFz7QPoxqBdUV4iugS3MX",
            "spotify.isrc": "GBBKS1400215",
            "spotify.popularity": 38,
            "spotify.title": "The Light",
            "title": "The Light",
            "calliope.spotify.resolver_score": 0.9999929688533533,
        }
    ]


def test_resolve_track_with_id(cli, mock_server):
    mock_server(
        {
            "track:The Light artist:SBTRKT": "spotify.track:The Light artist:SBTRKT.json",
            "The Light SBTRKT": "spotify.The Light SBTRKT.json",
            "SBTRKT": "spotify.SBTRKT.json",
        }
    )

    playlist = [
        {
            "title": "The Light (feat. Denai Moore)",
            "creator": "SBTRKT",
            "album": "Wonder Where We Land",
            "spotify.id": "1lFz7QPoxqBdUV4iugS3MX",
        }
    ]

    result = cli.run(["resolve", "--update", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()
    assert output == [
        {
            "album": "Wonder Where We Land",
            "creator": "SBTRKT",
            "duration": 186436.0,
            "spotify.album": "Wonder Where We Land",
            "spotify.album_id": "4J9gt4YOazmavlYw4hMrfY",
            "spotify.albumartist": "SBTRKT",
            "spotify.artist": "SBTRKT",
            "spotify.artist_id": "1O10apSOoAPjOu6UhUNmeI",
            "spotify.date": "2014-09-29",
            "spotify.duration_ms": 186436.0,
            "spotify.id": "1lFz7QPoxqBdUV4iugS3MX",
            "spotify.isrc": "GBBKS1400215",
            "spotify.popularity": 38,
            "spotify.title": "The Light",
            "title": "The Light",
            "calliope.spotify.resolver_score": 0.9999929688533533,
        }
    ]


def test_resolve_track_with_artist_id(cli, mock_server):
    mock_server(
        {
            "track:The Light artist:SBTRKT": "spotify.track:The Light artist:SBTRKT.json",
            "The Light SBTRKT": "spotify.The Light SBTRKT.json",
            "SBTRKT": "spotify.SBTRKT.json",
        }
    )

    playlist = [
        {
            "title": "The Light (feat. Denai Moore)",
            "creator": "SBTRKT",
            "album": "Wonder Where We Land",
            "spotify.artist_id": "1O10apSOoAPjOu6UhUNmeI",
        }
    ]

    result = cli.run(["resolve", "--update", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()
    assert output == [
        {
            "album": "Wonder Where We Land",
            "creator": "SBTRKT",
            "duration": 186436.0,
            "spotify.album": "Wonder Where We Land",
            "spotify.album_id": "4J9gt4YOazmavlYw4hMrfY",
            "spotify.albumartist": "SBTRKT",
            "spotify.artist": "SBTRKT",
            "spotify.artist_id": "1O10apSOoAPjOu6UhUNmeI",
            "spotify.date": "2014-09-29",
            "spotify.duration_ms": 186436.0,
            "spotify.id": "1lFz7QPoxqBdUV4iugS3MX",
            "spotify.isrc": "GBBKS1400215",
            "spotify.popularity": 38,
            "spotify.title": "The Light",
            "title": "The Light",
            "calliope.spotify.resolver_score": 0.9993178805806098,
        }
    ]


def test_resolve_album(cli, mock_server):
    mock_server(
        {
            "artist:The Burning Hell album:Public Library": "spotify.artist:The Burning Hell album:Public Library.json",
            "The Burning Hell Public Library": "spotify.The Burning Hell Public Library.json",
            "The Burning Hell": "spotify.The Burning Hell.json",
        }
    )

    playlist = [
        {
            "album": "Public Library",
            "creator": "The Burning Hell",
        }
    ]

    result = cli.run(["resolve", "--update", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()
    assert output == [
        {
            "album": "Public Library",
            "creator": "The Burning Hell",
            "spotify.album": "Public Library",
            "spotify.album_id": "36kt9AFzp9bueyRRBy6YmV",
            "spotify.albumartist": "The Burning Hell",
            "spotify.artist_id": "09kohMK0MSHgpmKWiQeQ5E",
            "spotify.date": "2016-04-01",
            "calliope.spotify.resolver_score": 0.9968340117593849,
        }
    ]


def test_resolve_artist(cli, mock_server):
    mock_server(
        {
            "artist:Röyksopp": "spotify.artist:Röyksopp.json",
            "Röyksopp": "spotify.Röyksopp.json",
        }
    )

    playlist = [
        {
            "creator": "Röyksopp",
        }
    ]

    result = cli.run(["resolve", "--update", "-"], input_playlist=playlist)
    result.assert_success()

    output = result.json()
    assert output == [
        {
            "calliope.spotify.resolver_score": 1.0,
            "creator": "Röyksopp",
            "spotify.artist": "Röyksopp",
            "spotify.artist_id": "5nPOO9iTcrs9k6yFffPxjH",
        }
    ]
