Creating mixes
==============

The :mod:`calliope.play` module and :command:`cpe play` command can render
a playlist to an audio file.
